<?php

namespace Tests;

use DateTime;
use XLSXLight\Cell;
use XLSXLight\Font;
use PHPUnit\Framework\TestCase;
use XLSXLight\Image;
use XLSXLight\Sheet;
use XLSXLight\Style;
use XLSXLight\Workbook;
use ZipArchive;

class WorkbookTests extends TestCase
{
    protected static $testDir;
    protected static $expectedDir;
    protected static $document = 'testing.xlsx';
    protected static $files = [
        '[Content_Types].xml',
        'docProps/app.xml',
        'xl/workbook.xml',
        'xl/sharedStrings.xml',
        'xl/styles.xml',
        'xl/drawings/drawing1.xml',
        'xl/drawings/_rels/drawing1.xml.rels',
        'xl/_rels/workbook.xml.rels',
        'xl/worksheets/sheet1.xml',
        'xl/worksheets/_rels/sheet1.xml.rels',
    ];
    protected static $dirs = [
        'docProps/',
        'xl/worksheets/_rels/',
        'xl/media/',
        'xl/_rels/',
        'xl/drawings/_rels/',
        'xl/drawings/',
        'xl/worksheets/',
        'xl/',
        ''
    ];

    public static function setUpBeforeClass()
    {
        self::$testDir = dirname(__FILE__) . '/actual/';
        self::$expectedDir = dirname(__FILE__) . '/expected/';

        $XLSX = new Workbook();
        //Styles
        $style = new Style();
        $style
            ->setNumberFormat('default', '[green]#,##0.00##;[Red]-#,##0.00##')
            ->setNumberFormat('currency', '"€" #,##0.00;[Red]-"€"\ #,##0.00')
            ->setNumberFormat('datetime', 'dd/mm/yyyy hh:mm:ss;@')
            ->setNumberFormat('date', 'dd/mm/yyyy;@')
            ->setNumberFormat('time', 'hh:mm:ss;@')
            ->setNumberFormat('percent', '#,##0.00 %;[Red]-#,##0.00 %')
            ->setFont((new Font('title'))
                ->setSize(18)
                ->setColor('FFFFFF')
                ->setAlignment('center center'))
            ->setFont((new Font('label'))
                ->setSize(14)
                ->setColor('FFFFFF')
                ->setBold()
                ->setItalic()
                ->setAlignment('center bottom')
            )
            ->setFill('gray', 'CCCCCC')
            ->setFill('blue', '003399')
            ->setBorder('fullBorder', 'thin thin thin thick', '000000')
            ->setBorder('fullGrid', 'thin', '000000');

        $XLSX->setStyle($style);

        $sheet = (new Sheet('DirectWrite', $XLSX))
            ->setDefaultColWidth(120)
            ->setDefaultRowHeight(20)
            ->setColumnWidth('B:C', 140)
            ->setColumnWidth('A', 80)
            ->freezePanes('A6')
            ->setCell((new Cell('B2:C3', 'XLSXLight Report Example'))
                ->setFont('title')
                ->setFill('blue')
            )
            ->addImage((new Image('D2', self::$expectedDir . 'xl/media/image1.png'))->setSize(240))
            ->setCell((new Cell('B5', 'Format'))->setFont('label')->setBorder('fullBorder')->setFill('gray'))
            ->setCell((new Cell('C5', 'Value'))->setFont('label')->setBorder('fullBorder')->setFill('gray'));

        $sheet
            ->setCell((new Cell('B6', 'default'))->setBorder('fullGrid'))
            ->setCell((new Cell('C6', 2543.21))->setBorder('fullGrid'))
            ->setCell((new Cell('B7', 'currency'))->setBorder('fullGrid'))
            ->setCell((new Cell('C7', 3543.21))->setFormat('currency')->setBorder('fullGrid'))
            ->setCell((new Cell('B8', 'percent'))->setBorder('fullGrid'))
            ->setCell((new Cell('C8', 4543.213424))->setFormat('percent')->setBorder('fullGrid'))
            ->setCell((new Cell('B9', 'DateTime'))->setBorder('fullGrid'))
            ->setCell((new Cell('C9', new DateTime('30-08-2017 23:50:50')))->setBorder('fullGrid'))
            ->setCell((new Cell('B10', 'Date'))->setBorder('fullGrid'))
            ->setCell((new Cell('C10', new DateTime('30-08-2017 23:50:50')))->setFormat('date')->setBorder('fullGrid'))
            ->setCell((new Cell('B11', 'Time'))->setBorder('fullGrid'))
            ->setCell((new Cell('C11', new DateTime('30-08-2017 23:50:50')))->setFormat('time')->setBorder('fullGrid'));

        $XLSX->save(self::$testDir . self::$document);
    }

    public static function tearDownAfterClass()
    {
        unlink(self::$testDir . self::$document);
        foreach (self::$dirs as $dir) {
            $files = scandir(self::$testDir . $dir);
            foreach ($files as $file) {
                if (is_file(self::$testDir . $dir . $file)) {
                    unlink(self::$testDir . $dir . $file);
                }
            }
            if ($dir != '') {
                rmdir(self::$testDir . $dir);
            }
        }
    }

    public function testWorkbookExists()
    {
        $this->assertFileExists(self::$testDir . self::$document);
    }

    public function testFiles()
    {
        $zip = new ZipArchive();
        $zip->open(self::$testDir . self::$document);

        foreach (self::$files as $file) {
            $exists = $zip->locateName($file) !== false;
            $this->assertTrue($exists, $file . ' is missing from Workbook');
            if ($exists) {
                $zip->extractTo(self::$testDir, [$file]);
                $this->assertXmlFileEqualsXmlFile(
                    self::$expectedDir . $file,
                    self::$testDir . $file,
                    $file . ' does not match expected file!'
                );
            }
        }
        $file = 'xl/media/image1.png';
        $exists = $zip->locateName($file) !== false;
        $this->assertTrue($exists, $file . ' is missing from Workbook');
        if ($exists) {
            $zip->extractTo(self::$testDir, [$file]);
            $this->assertFileEquals(
                self::$expectedDir . $file,
                self::$testDir . $file,
                $file . ' Image appears to be different!'
            );
        }
        $zip->close();
    }

    public function testColumnConverter()
    {
        $this->assertEquals('DT', Workbook::getColumnLetter(123));
        $this->assertEquals(27, Workbook::getColumnNumber('AB'));
    }
}
