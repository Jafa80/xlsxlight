<?php

namespace Tests;

use XLSXLight\Font;
use PHPUnit\Framework\TestCase;
use XLSXLight\Style;
use XLSXLight\Xml\StylesXml;

class FontTests extends TestCase
{
    /** @var  Font $font */
    private $font;

    protected function setUp()
    {
        $this->font = new Font('testFont');
    }

    protected function tearDown()
    {
        $this->font = null;
    }

    public function testTagName()
    {
        $this->assertEquals('testFont', $this->font->getTag());
    }

    public function testAlignment()
    {
        $this->font->setAlignment('left center');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'left', 'vertical' => 'center']);
        $this->font->setAlignment('left top');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'left', 'vertical' => 'top']);
        $this->font->setAlignment('left bottom');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'left', 'vertical' => 'bottom']);

        $this->font->setAlignment('right center');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'right', 'vertical' => 'center']);
        $this->font->setAlignment('right top');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'right', 'vertical' => 'top']);
        $this->font->setAlignment('right bottom');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'right', 'vertical' => 'bottom']);

        $this->font->setAlignment('center center');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'center', 'vertical' => 'center']);
        $this->font->setAlignment('center top');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'center', 'vertical' => 'top']);
        $this->font->setAlignment('center bottom');
        $this->assertTrue($this->font->getAlignment() == ['horizontal' => 'center', 'vertical' => 'bottom']);
    }

    public function testAlignmentException()
    {
        $this->expectException('Exception');
        $this->font->setAlignment('top center');
    }

    public function testFontStyle()
    {
        $this->font->setBold()
            ->setCharset(187)
            ->setName('Arial')
            ->setColor('CCCCCC')
            ->setItalic()
            ->setSize(18)
            ->setStrikeThrow()
            ->setUndeline()
            ->setFamily(3)
            ->setScheme('minor');

        $xml = '<font><b/><u val="1"/><i/><strike/><sz val="18"/><name val="Arial"/><family val="3"/>'
            . '<charset val="187"/><scheme val="minor"/><color rgb="CCCCCC"/></font>';
        $this->assertEquals($xml, (new StylesXml(new Style()))->getFontStyleXml($this->font));
    }
}
