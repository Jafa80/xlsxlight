<?php
/**
 * Created by PhpStorm.
 * User: janis
 * Date: 29/08/2017
 * Time: 23:10
 */

namespace Tests;

use XLSXLight\Border;
use PHPUnit\Framework\TestCase;
use XLSXLight\Style;
use XLSXLight\Xml\StylesXml;

class BorderTests extends TestCase
{
    public function testBorderThickness()
    {
        $borderXml = new StylesXml(new Style());

        $border = new Border('thin', '000000');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="000000"/></right>';
        $xml .= '<top style="thin"><color rgb="000000"/></top>';
        $xml .= '<bottom style="thin"><color rgb="000000"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

        $border = new Border('thick', '000000');
        $xml = '<border>';
        $xml .= '<left style="thick"><color rgb="000000"/></left>';
        $xml .= '<right style="thick"><color rgb="000000"/></right>';
        $xml .= '<top style="thick"><color rgb="000000"/></top>';
        $xml .= '<bottom style="thick"><color rgb="000000"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

        $border = new Border('thin thick', '000000');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="000000"/></right>';
        $xml .= '<top style="thick"><color rgb="000000"/></top>';
        $xml .= '<bottom style="thick"><color rgb="000000"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

        $border = new Border('thin thick thin none', '000000');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="000000"/></right>';
        $xml .= '<top style="thick"><color rgb="000000"/></top>';
        $xml .= '<bottom/>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

    }

    public function testBorderColor()
    {
        $borderXml = new StylesXml(new Style());

        $border = new Border('thin', '000000');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="000000"/></right>';
        $xml .= '<top style="thin"><color rgb="000000"/></top>';
        $xml .= '<bottom style="thin"><color rgb="000000"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

        $border = new Border('thin', '000000 CCCCCC');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="000000"/></right>';
        $xml .= '<top style="thin"><color rgb="CCCCCC"/></top>';
        $xml .= '<bottom style="thin"><color rgb="CCCCCC"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));

        $border = new Border('thin', '000000 CCCCCC FFFFFF none');
        $xml = '<border>';
        $xml .= '<left style="thin"><color rgb="000000"/></left>';
        $xml .= '<right style="thin"><color rgb="FFFFFF"/></right>';
        $xml .= '<top style="thin"><color rgb="CCCCCC"/></top>';
        $xml .= '<bottom style="thin"><color auto="1"/></bottom>';
        $xml .= '<diagonal/>';
        $xml .= '</border>';
        $this->assertEquals($xml, $borderXml->getBorderXml($border));
    }

    public function testBorderThicknessException()
    {
        $this->expectException('Exception');
        new Border('thin thick thin', '000000');
    }

    public function testBorderColorException()
    {
        $this->expectException('Exception');
        new Border('thin', '000000 000000 000000');
    }
}
