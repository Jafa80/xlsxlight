<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

use XLSXLight\Cell;
use XLSXLight\Font;
use XLSXLight\Image;
use XLSXLight\Sheet;
use XLSXLight\Style;
use XLSXLight\Workbook;

require '../vendor/autoload.php';

$XLSX = new Workbook();

$style = new Style();
//XLSXLight has preset default styles for
// number, currency, date, time and datetime
// these can be overwritten
$style
    //Allways use "." to define decimal separator and "," for thousand separator
    //Excel is using OS regional settings format for it to work properly
    ->setNumberFormat('default', '[green]#,##0.00##;[Red]-#,##0.00##')
    ->setNumberFormat('currency', '"€" #,##0.00;[Red]-"€"\ #,##0.00')
    //date and time are more flexible. you can format them any way you want.
    ->setNumberFormat('datetime', 'dd/mm/yyyy hh:mm:ss;@')
    ->setNumberFormat('date', 'dd/mm/yyyy;@')
    ->setNumberFormat('time', 'hh:mm:ss;@')
    ->setNumberFormat('percent', '#,##0.00 %;[Red]-#,##0.00 %')
    //Add fonts
    ->setFont((new Font('title'))
        ->setSize(18)
        ->setColor('FFFFFF')
        ->setAlignment('center center'))
    ->setFont((new Font('label'))
        ->setSize(14)
        ->setColor('FFFFFF')
        ->setBold()
        ->setItalic()
        ->setAlignment('center bottom') //first horizontal, the vertical
    )
    //define fills to highlight cells
    ->setFill('gray','CCCCCC')
    ->setFill('blue','003399')
    //define borders
    ->setBorder('fullBorder','thin thin thin thick','000000')
    ->setBorder('fullGrid','thin','000000');

//Apply style to the document
$XLSX->setStyle($style);

//By default each sheet has directWrite enabled
//directWrite writes rows directly to the file and locks all the previous rows for further writing.
$sheet = (new Sheet('DirectWrite', $XLSX))
    ->setDefaultColWidth(120)
    ->setDefaultRowHeight(20)
    ->setColumnWidth('B:C',140)
    ->setColumnWidth('A',80)
    //Activate pane freeze by defining frozen panes left top corner
    ->freezePanes('A6')
    ->setCell((new Cell('B2:C3', 'XLSXLight Report Example'))
        ->setFont('title')
        ->setFill('blue')
    )
    ->addImage((new Image('D2','logo.png'))->setSize(240))
    //Set labels row
    ->setCell((new Cell('B5', 'Format'))->setFont('label')->setBorder('fullBorder')->setFill('gray'))
    ->setCell((new Cell('C5', 'Value'))->setFont('label')->setBorder('fullBorder')->setFill('gray'));

$sheet
    ->setCell((new Cell('B6', 'default'))->setBorder('fullGrid'))
    ->setCell((new Cell('C6', 2543.21))->setBorder('fullGrid'))
    ->setCell((new Cell('B7', 'currency'))->setBorder('fullGrid'))
    ->setCell((new Cell('C7', 3543.21))->setFormat('currency')->setBorder('fullGrid'))
    ->setCell((new Cell('B8', 'percent'))->setBorder('fullGrid'))
    ->setCell((new Cell('C8', 4543.213424))->setFormat('percent')->setBorder('fullGrid'))
    ->setCell((new Cell('B9', 'DateTime'))->setBorder('fullGrid'))
    // Passing datetime object will set the field format default to datetime
    ->setCell((new Cell('C9', new DateTime()))->setBorder('fullGrid'))
    ->setCell((new Cell('B10', 'Date'))->setBorder('fullGrid'))
    ->setCell((new Cell('C10', new DateTime()))->setFormat('date')->setBorder('fullGrid'))
    ->setCell((new Cell('B11', 'Time'))->setBorder('fullGrid'))
    ->setCell((new Cell('C11', new DateTime()))->setFormat('time')->setBorder('fullGrid'));


/* Create new Sheet and fill all the data  this time we are not using directWrite */
/* AS the document will be generated only when all the data is set, we do not have
to worry about the order we set the data for this sheet */
$sheet = (new Sheet('no DirectWrite', $XLSX, false))
    ->setDefaultColWidth(120)
    ->setDefaultRowHeight(20)
    ->setColumnWidth('B:C',140)
    ->setColumnWidth('A',80)
    ->setCell((new Cell('B2:C3', 'XLSXLight Report Example'))
        ->setFont('title')
        ->setFill('blue')
    )
    ->setCell((new Cell('B4','On this sheet we have Direct write disabled!')))
    ->setCell((new Cell('C9', new DateTime()))->setBorder('fullGrid'))
    ->setCell((new Cell('B10', 'Date'))->setBorder('fullGrid'))
    ->setCell((new Cell('C10', new DateTime()))->setFormat('date')->setBorder('fullGrid'))
    ->setCell((new Cell('B6', 'default'))->setBorder('fullGrid'))
    ->setCell((new Cell('C6', -543.21))->setBorder('fullGrid'))
    ->setCell((new Cell('B7', 'currency'))->setBorder('fullGrid'))
    ->setCell((new Cell('C7', 543.21))->setFormat('currency')->setBorder('fullGrid'))
    ->setCell((new Cell('B8', 'percent'))->setBorder('fullGrid'))
    ->setCell((new Cell('C8', 543.21))->setFormat('percent')->setBorder('fullGrid'))
    ->setCell((new Cell('B9', 'DateTime'))->setBorder('fullGrid'))
    ->setCell((new Cell('B11', 'Time'))->setBorder('fullGrid'))
    ->setCell((new Cell('C11', new DateTime()))->setFormat('time')->setBorder('fullGrid'))
    ->setCell((new Cell('B5', 'Format'))->setFont('label')->setBorder('fullBorder')->setFill('gray'))
    ->setCell((new Cell('C5', 'Value'))->setFont('label')->setBorder('fullBorder')->setFill('gray'));
/* */


$filename = 'sample.xlsx';

/* Write the file for download *
header('Content-Disposition: attachment; filename="' . $filename . '"');
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
$XLSX->stdOut();
/**/

/* Or save it on server */
$XLSX->save($filename);
/**/