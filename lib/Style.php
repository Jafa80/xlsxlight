<?php

namespace XLSXLight;

use Exception;
use stdClass;

/**
 * @property Border[] $borders;
 */
class Style
{
    private $formatIndex = 165;
    private $fillIndex = 2;

    private $fonts = [];
    private $styleSet = [];
    private $numFormat = [];
    private $borders = [];
    private $fills = [];
    private $slashedChars = ' ';

    function __construct()
    {
        //add default font
        $this->setFont(new Font('default'));

        //add default number formats
        $this->setNumberFormat('default', '#,##0.00');
        $this->setNumberFormat('currency', '#,##0.00 "€"');
        $this->setNumberFormat('datetime', 'dd/mm/yyyy hh:mm;@');
        $this->setNumberFormat('date', 'dd/mm/yyyy;@');
        $this->setNumberFormat('time', 'hh:mm:ss;@');
        $this->setNumberFormat('percent', '#,##0.00 %');
    }

    public function setFont(Font $font)
    {
        $this->fonts[$font->getTag()] = $font;
        return $this;
    }

    public function setNumberFormat($formatTag, $format)
    {
        if (isset($this->numFormat[$formatTag])) {
            $formatOb = $this->numFormat[$formatTag];
        } else {
            $formatOb = new stdClass();
            $formatOb->id = $this->formatIndex++;
        }
        $formatOb->format = addcslashes($format, $this->slashedChars);
        $this->numFormat[$formatTag] = $formatOb;
        return $this;
    }

    public function setFill($fillTag, $rgbColor)
    {
        if (isset($this->fills[$fillTag])) {
            $fill = $this->fills[$fillTag];
        } else {
            $fill = new stdClass();
            $fill->id = $this->fillIndex++;
        }
        $fill->color = ltrim($rgbColor, '#');
        $this->fills[$fillTag] = $fill;
        return $this;
    }

    public function getFillsList()
    {
        return $this->fills;
    }

    public function setBorder($borderTag, $styles, $colors)
    {
        $id = count($this->borders);
        if (!isset($this->borders[$borderTag])) {
            $id++;
        }
        $this->borders[$borderTag] = (new Border($styles, $colors))->setId($id);
        return $this;
    }

    public function getStyle($font, $numfmt, $fill, $border)
    {
        $fontId = $this->getFontId(strval($font));
        $formatId = (!$numfmt) ? 0 : $this->getFormatId($numfmt);
        $fillId = $this->getFillId($fill);
        $borderId = (!$border) ? 0 : $this->getBorderId($border);
        $alignment = $this->fonts[$font]->getAlignment();
        $horAlign = (isset($alignment['horizontal']) ? $alignment['horizontal'] : 0);
        $verAlign = (isset($alignment['vertical']) ? $alignment['vertical'] : 0);

        $index = $formatId . '_' . $fontId . '_' . $fillId . '_' . $borderId . '_' . $horAlign . '_' . $verAlign;
        if (isset($this->styleSet[$index])) {
            $value = $this->styleSet[$index]->id;
        } else {
            $value = (count($this->styleSet) + 1);//increment count by 1, 0 is taken by default style;
            $style = new stdClass();
            $style->id = $value;
            $style->nformat = $formatId;
            $style->font = $fontId;
            $style->fill = $fillId;
            $style->border = $borderId;
            $style->alignment = $alignment;
            $this->styleSet[$index] = $style;
        }
        return $value;
    }

    public function getStyleSet()
    {
        return $this->styleSet;
    }

    public function getFontId($fontTag)
    {
        $fontId = array_search($fontTag, array_keys($this->fonts));
        if ($fontId === false) {
            throw new Exception('Missing font for ' . $fontTag);
        }
        return $fontId;
    }

    public function getFillId($fillTag)
    {
        return isset($this->fills[$fillTag]) ? $this->fills[$fillTag]->id : 0;
    }

    public function getFormatId($numFmt)
    {
        return isset($this->numFormat[$numFmt]) ? $this->numFormat[$numFmt]->id : 0;
    }

    public function getFormatList()
    {
        return $this->numFormat;
    }

    public function getFontList()
    {
        return $this->fonts;
    }

    public function getBorderId($border)
    {
        return isset($this->borders[$border]) ? $this->borders[$border]->getId() : 0;
    }

    public function getBordersList()
    {
        return $this->borders;
    }

    public static function isRGBColor($RGBColorCode)
    {
        return ctype_xdigit($RGBColorCode) && mb_strlen($RGBColorCode) == 6;
    }
}