<?php

namespace XLSXLight;

use XLSXLight\Xml\AppXml;
use XLSXLight\Xml\ContentTypeXml;
use XLSXLight\Xml\CoreXml;
use XLSXLight\Xml\ExRelsXml;
use XLSXLight\Xml\RelsXml;
use XLSXLight\Xml\SharedStringsXml;
use XLSXLight\Xml\SheetDrawingRelsXml;
use XLSXLight\Xml\SheetDrawingsXml;
use XLSXLight\Xml\SheetRelsXml;
use XLSXLight\Xml\SheetXml;
use XLSXLight\Xml\StylesXml;
use XLSXLight\Xml\WorkbookXml;
use ZipArchive;

/**
 * Class XLSXLight
 * @property string $WorkbookAuthor
 * @property ObjectIterator $sheets
 * @property Style $style
 * @property integer $activeSheet
 * @property ObjectIterator $sharedStrings
 * @property integer $sharedStringsCount
 * @property array $sharedImages
 * @const COLUMN_UNIT_RATIO
 */
class Workbook
{
    protected $WorkbookAuthor = 'DOC Author';
    public $sheets;
    protected $style;
    private $activeSheet = 1;
    private $sharedStrings;
    private $sharedStringsCount = 0;
    private $sharedImages = [];
    const COLUMN_UNIT_RATIO = 6;

    function __construct()
    {
        $this->sheets = new ObjectIterator();
        $this->sharedStrings = new ObjectIterator();
        $this->style = new Style();
    }

    function setStyle(Style $style)
    {
        $this->style = $style;
    }

    /**
     * @return Style
     */
    function getStyle()
    {
        return $this->style;
    }

    public function setAuthor($authorName)
    {
        $this->WorkbookAuthor = $authorName;

        return $this;
    }

    public function getAuthor()
    {
        return $this->WorkbookAuthor;
    }

    public function setActiveSheet($sheetId)
    {
        $this->activeSheet = $sheetId;

        return $this;
    }

    public static function getColumnLetter($number)
    {
        for ($letter = ""; $number >= 0; $number = intval($number / 26) - 1) {
            $letter = chr($number % 26 + 0x41) . $letter;
        }

        return $letter;
    }

    public static function getColumnNumber($columnLetter)
    {
        $columnLetters = str_split(strtoupper($columnLetter));
        $alphabet = array_flip(range('A', 'Z'));
        $value = 0;
        foreach (array_reverse($columnLetters) as $index => $letter) {
            $alphaIndex = $alphabet[$letter] + 1;
            $value += $alphaIndex * pow(26, $index);
        }

        return $value - 1;
    }

    public function getActiveSheet()
    {
        return $this->activeSheet;
    }

    public function addSharedString($string)
    {
        $this->sharedStringsCount++;
        if (!$this->sharedStrings->hasItem($string)) {
            $this->sharedStrings->addItem($this->sharedStrings->count(), $string);
        }

        return $this->sharedStrings->getItem($string);
    }

    public function getSharedStrings()
    {
        return $this->sharedStrings;
    }

    public function getSharedStringsCount()
    {
        return $this->sharedStringsCount;
    }

    private function addSharedImage($filePath)
    {
        if ($existing = array_search($filePath, $this->sharedImages)) {
            return $existing;
        }
        $index = count($this->sharedImages) + 1;
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        $this->sharedImages['image' . $index . '.' . $ext] = $filePath;

        return 'image' . $index . '.' . $ext;
    }

    public function save($filename)
    {
        @unlink($filename);
        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE) != true) {
            die ("Could not open archive");
        }
        (new CoreXml($this))->save($zip);
        (new AppXml())->save($zip);
        (new AppXml())->save($zip);
        (new ContentTypeXml($this))->save($zip);
        (new RelsXml())->save($zip);
        (new WorkbookXml($this))->save($zip);
        (new ExRelsXml($this))->save($zip);

        /** @var Sheet $sheet */
        foreach ($this->sheets as $sheet) {
            (new SheetXml($sheet))->save($zip);
            $images = $sheet->getImages();
            if (!empty($images)) {
                $sheet->adjustImagePositions();
                /** @var Image $image */
                foreach ($images as $image) {
                    $sharedImage = $this->addSharedImage($image->getOriginImage());
                    $zip->addFile($image->getOriginImage(), 'xl/media/' . $sharedImage);
                    $image->setSharedImage($sharedImage);
                }
                (new SheetRelsXml($sheet))->save($zip);
                (new SheetDrawingsXml($sheet))->save($zip);
                (new SheetDrawingRelsXml($sheet))->save($zip);
            }
        }

        (new StylesXml($this->style))->save($zip);
        if ($this->sharedStrings->count() !== 0) {
            (new SharedStringsXml($this))->save($zip);
        }

        $zip->close();
    }

    public function stdOut()
    {
        $filename = tempnam("", "xlsxlight_workbook");
        $this->save($filename);
        readfile($filename);
    }
}