<?php

namespace XLSXLight;

use Exception;

class Font
{
    private $tag = null;
    private $name = 'Calibri';
    private $size = 11;
    private $color = null;
    private $bold = false;
    private $italic = false;
    private $underline = false;
    private $charset = 186;
    private $family = 2;
    private $scheme = 'minor';
    private $strikeThrow = false;
    private $alignment = null;

    function __construct($fontTag)
    {
        $this->tag = $fontTag;
    }

    public function setName($fontName)
    {
        $this->name = $fontName;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    //here goes number
    public function setFamily($family)
    {
        $this->family = $family;
        return $this;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function setSize($number)
    {
        $this->size = $number;
        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setBold()
    {
        $this->bold = true;
        return $this;
    }

    public function isBold()
    {
        return $this->bold;
    }

    public function setItalic()
    {
        $this->italic = true;
        return $this;
    }

    public function isItalic()
    {
        return $this->italic;
    }

    public function setUndeline()
    {
        $this->underline = true;
        return $this;
    }

    public function isUnderline()
    {
        return $this->underline;
    }

    public function setStrikeThrow()
    {
        $this->strikeThrow = true;
        return $this;
    }

    public function isStrikeThrow()
    {
        return $this->strikeThrow;
    }

    public function setColor($rgbColor)
    {
        $this->color = Style::isRGBColor($rgbColor)
            ? $rgbColor
            : null;
        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    public function getCharset()
    {
        return $this->charset;
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
        return $this;
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * first horizontal [left|center|right] then vertical [top|center|bottom]
     * ex: left bottom
     * @param string $alignment
     * @return $this
     * @throws Exception
     */
    public function setAlignment($alignment)
    {
        if (!preg_match('/^(left|center|right)[ ]?(top|center|bottom)?$/', $alignment, $matches)) {
            throw new Exception('Incorrect alignment parameter ' . $alignment);
        }
        $this->alignment['horizontal'] = $matches[1];
        if (isset($matches[2])) {
            $this->alignment['vertical'] = $matches[2];
        }
        return $this;
    }

    public function getAlignment()
    {
        return $this->alignment;
    }

    public function getTag()
    {
        return $this->tag;
    }
}