<?php

namespace XLSXLight;

class Border
{
    private $lineStyles = [
        'thin',
        'thick',
    ];

    private $id;

    private $styleLeft = null;
    private $styleTop = null;
    private $styleRight = null;
    private $styleBottom = null;

    private $colorLeft = null;
    private $colorTop = null;
    private $colorRight = null;
    private $colorBottom = null;

    /**
     * Border constructor.
     * @param string $styles
     * @param string $colors
     */
    function __construct($styles, $colors)
    {
        $this->setLineThickness(explode(' ', $styles))
            ->setLineColors(explode(' ', $colors));
    }

    /**
     * @param array $styles
     * @return $this
     * @throws \Exception
     */
    protected function setLineThickness($styles)
    {
        switch (count($styles)) {
            case 0:
                $this->styleLeft
                    = $this->styleTop
                    = $this->styleRight
                    = $this->styleBottom
                    = null;
                break;
            case 1:
                $this->styleLeft
                    = $this->styleTop
                    = $this->styleRight
                    = $this->styleBottom
                    = in_array($styles[0], $this->lineStyles) ? $styles[0] : null;
                break;
            case 2:
                $this->styleLeft
                    = $this->styleRight
                    = in_array($styles[0], $this->lineStyles) ? $styles[0] : null;
                $this->styleTop
                    = $this->styleBottom
                    = in_array($styles[1], $this->lineStyles) ? $styles[1] : null;
                break;
            case 4:
                $this->styleLeft = in_array($styles[0], $this->lineStyles) ? $styles[0] : null;
                $this->styleTop = in_array($styles[1], $this->lineStyles) ? $styles[1] : null;
                $this->styleRight = in_array($styles[2], $this->lineStyles) ? $styles[2] : null;
                $this->styleBottom = in_array($styles[3], $this->lineStyles) ? $styles[3] : null;
                break;
            default:
                throw new \Exception('Illegal number of line styles');
                break;
        }
        return $this;
    }

    /**
     * @param array $colors
     * @return $this
     * @throws \Exception
     */
    protected function setLineColors($colors)
    {
        switch (count($colors)) {
            case 0:
                $this->colorLeft
                    = $this->colorTop
                    = $this->colorRight
                    = $this->colorBottom
                    = null;
                break;
            case 1:
                $this->colorLeft
                    = $this->colorTop
                    = $this->colorRight
                    = $this->colorBottom
                    = Style::isRGBColor($colors[0]) ? $colors[0] : null;
                break;
            case 2:
                $this->colorLeft
                    = $this->colorRight
                    = Style::isRGBColor($colors[0]) ? $colors[0] : null;
                $this->colorTop
                    = $this->colorBottom
                    = Style::isRGBColor($colors[1]) ? $colors[1] : null;
                break;
            case 4:
                $this->colorLeft = Style::isRGBColor($colors[0]) ? $colors[0] : null;
                $this->colorTop = Style::isRGBColor($colors[1]) ? $colors[1] : null;
                $this->colorRight = Style::isRGBColor($colors[2]) ? $colors[2] : null;
                $this->colorBottom = Style::isRGBColor($colors[3]) ? $colors[3] : null;
                break;
            default:
                throw new \Exception('Illegal number of line colors');
                break;
        }
        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBorderStyles()
    {
        return [
            'left' => $this->styleLeft,
            'top' => $this->styleTop,
            'right' => $this->styleRight,
            'bottom' => $this->styleBottom,
        ];
    }

    public function getBorderColors()
    {
        return [
            'left' => $this->colorLeft,
            'top' => $this->colorTop,
            'right' => $this->colorRight,
            'bottom' => $this->colorBottom,
        ];
    }
}