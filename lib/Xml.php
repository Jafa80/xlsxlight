<?php

namespace XLSXLight;


abstract class Xml
{
    protected $version = '1.0';
    protected $charset = 'UTF-8';
    protected $standalone = true;

    abstract protected function getFileName();

    abstract public function getXml();

    protected function xmlSpecialChars($val)
    {
        return strtr(
            htmlspecialchars($val, ENT_QUOTES | ENT_XML1),
            "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x7f",
            ""
        );
    }

    protected function getContentTypeXML()
    {
        return '<?xml version="' . $this->version . '" encoding="' . $this->charset . '" standalone="'
            . ($this->standalone ? 'yes' : 'no')
            . '"?>' . PHP_EOL;
    }

    public function save(\ZipArchive $zip)
    {
        $fileName = $this->getFileName();
        if (!empty($fileName)) {
            $zip->addFromString($fileName, $this->getXML());
        }
    }
}