<?php

namespace XLSXLight;

use Exception;
use XLSXLight\Xml\SheetXml;

class Sheet
{
    private $title;
    private $WORKBOOK;
    private $id;
    private $defaultRowHeight = 20;
    private $defaultColWidth = 10;
    private $tempFile;
    private $directWrite = true;
    private $lockOut;
    private $data;
    private $images = [];
    private $columnWidth = [];
    private $mergedCells = [];
    private $paneFreeze;
    private $xml;

    function __construct($title, Workbook $workbook, $directWrite = true)
    {
        $this->id = ($workbook->sheets->count() + 1);
        $workbook->sheets->addItem($this);
        $this->WORKBOOK = $workbook;
        $this->data = new ObjectIterator();
        $this->title = $title;
        $this->directWrite = $directWrite;
        $this->tempFile = tempnam('', 'xlsxlight_sheet');
        $this->xml = new SheetXml($this);
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function freezePanes($cellIndex)
    {
        $this->paneFreeze = $cellIndex;
        return $this;
    }

    public function getPaneFreez()
    {
        return $this->paneFreeze;
    }

    /**
     * @param int $height in pixels
     * @return $this
     */
    public function setDefaultRowHeight($height)
    {
        //Maximum row height in Excel is 409px
        $this->defaultRowHeight = ($height > 409) ? 409 : $height;
        return $this;
    }

    /**
     * @param int $width in pixels
     * @return $this
     */
    public function setDefaultColWidth($width)
    {
        //excel stores column with with the ratio 1/6 in pixels
        $this->defaultColWidth = $width / Workbook::COLUMN_UNIT_RATIO;
        return $this;
    }

    public function getDefaultRowHeight()
    {
        return $this->defaultRowHeight;
    }

    public function getDefaultColWidth()
    {
        return $this->defaultColWidth;
    }

    public function getMergedCells()
    {
        return $this->mergedCells;
    }

    public function setColumnWidth($column, $width)
    {
        $columns = explode(':', $column);
        $min = (Workbook::getColumnNumber($columns[0]) + 1);
        $count = (isset($columns[1])) ? (Workbook::getColumnNumber($columns[1]) + 1) - $min + 1 : 1;
        $range = array_fill($min, $count, round($width / Workbook::COLUMN_UNIT_RATIO, 7));
        $this->columnWidth = array_replace($this->columnWidth, $range);
        return $this;
    }

    public function getColumnWidth($columnNumber)
    {
        return (isset($this->columnWidth[$columnNumber]))
            ? $this->columnWidth[$columnNumber] * Workbook::COLUMN_UNIT_RATIO
            : $this->defaultColWidth * Workbook::COLUMN_UNIT_RATIO;
    }

    public function getColumnWidthList()
    {
        return $this->columnWidth;
    }

    public function getRangeWidth($column)
    {
        $columns = explode(':', $column);
        $min = (Workbook::getColumnNumber($columns[0]) + 1);
        $max = (isset($columns[1])) ? (Workbook::getColumnNumber($columns[1]) + 1) : $min;
        $width = 0;
        foreach (range($min, $max) as $number) {
            $width += $this->getColumnWidth($number);
        }
        return $width;
    }

    public function getTmpFile()
    {
        return $this->tempFile;
    }

    public function getWorkbook()
    {
        return $this->WORKBOOK;
    }

    private function generateData()
    {
        if (!$this->data->valid()) {
            return false;
        }
        $this->data->sort();
        while ($this->data->valid()) {
            $key = $this->data->key();
            $row = $this->data->current();
            if ($row->getCells()->count() === 0) {
                continue;
            }
            if (!$this->lockOut) {
                //Write sheet file heading before any data kets written to file.
                $this->xml->writeHeadXml();
            }
            $this->xml->writeRowXml($row);
            $this->lockOut = $key;
            $this->data->next();
            $this->data->remove($key);
        }
        return true;
    }

    public function getSheetFile()
    {
        $this->generateData();
        $this->xml->writeFootXml();
        return $this->tempFile;
    }

    /**
     * @param Cell $cell
     * @return $this
     * @throws Exception
     */
    public function setCell(Cell $cell)
    {
        if ($this->lockOut >= $cell->getRow()) {
            throw new Exception('Unable to set data for Cell ' . $cell->getIndex() . ', row is all ready locked!');
        } elseif (!$this->data->hasItem($cell->getRow())) {
            if ($this->data->count() > 0 && $this->directWrite) {
                $this->generateData();
            }
            $this->data->addItem(new Row(), $cell->getRow());
        }
        $existingRow = $this->data->getItem($cell->getRow());
        if (!$existingRow) {
            throw new Exception('Row not found' . $cell->getIndex());
        }
        $existingRow->addCell($cell);
        if ($cell->getMergeRange()) {
            $this->mergedCells[] = $cell->getMergeRange();
            foreach ($cell->getMergedRows() as $row) {
                foreach ($cell->getMergedColumns() as $col) {
                    if ($this->data->getItem($row) && $this->data->getItem($row)->hasCell(Workbook::getColumnLetter($col) . $row)) {
                        continue;
                    }
                    $this->setCell((new Cell(Workbook::getColumnLetter($col) . $row))
                        ->setBorder($cell->getBorder())
                        ->setFill($cell->getFill())
                    );
                }
            }
        }
        return $this;
    }

    public function mergeCells($range)
    {
        if (is_array($range)) {
            foreach ($range as $r) {
                $this->mergeCells($r);
            }
        }
        preg_match("/^(([A-Z]+)([0-9]+))[:]?(([A-Z]+)([0-9]+))?$/", $range, $match);
        if (sizeof($match) != 7) {
            throw new Exception('Invalid Cell range "' . $range . '"');
        }
        $this->mergedCells[] = $range;

        return $this;
    }

    public function addImage(Image $image)
    {
        $image->setId(count($this->images) + 1);
        $this->images[] = $image;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function adjustImagePositions()
    {
        /** @var Image $image */
        foreach ($this->images as $image) {
            $image->adjustPositionRelativeToSheet($this);
        }
    }
}