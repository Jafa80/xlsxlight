<?php
namespace XLSXLight;

/**
 * Class XLSXLightRow
 *
 * @property ObjectIterator $cells
 * @property integer $index
 */
class Row
{
    private $cells;
    private $index = null;

    public function __construct()
    {
        $this->cells = new ObjectIterator();
    }

    public function addCell(Cell $cell)
    {
        $this->cells->addItem($cell, $cell->getIndex());
        $this->index = $cell->getRow();
        return $this;
    }

    public function getIndex(){
        return $this->index;
    }

    public function getCells()
    {
        $this->cells->sort();
        return $this->cells;
    }

    public function hasCell($cellIndex)
    {
        return $this->cells->hasItem($cellIndex);
    }
}