<?php

namespace XLSXLight\Xml;


use XLSXLight\Sheet;
use XLSXLight\Xml;

class SheetRelsXml extends Xml
{

    protected $sheet;

    function __construct(Sheet $sheet)
    {
        $this->sheet = $sheet;
    }

    protected function getFileName()
    {
        return 'xl/worksheets/_rels/sheet' . $this->sheet->getId() . '.xml.rels';
    }

    public function getXml()
    {
        return $this->getContentTypeXML()
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">'
            . '<Relationship Id="rId1" '
            . 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing" '
            . 'Target="../drawings/drawing' . $this->sheet->getId() . '.xml"/>'
            . '</Relationships>';
    }
}