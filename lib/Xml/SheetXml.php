<?php

namespace XLSXLight\Xml;


use DateTime;
use XLSXLight\Cell;
use XLSXLight\Row;
use XLSXLight\Sheet;
use XLSXLight\Xml;

class SheetXml extends Xml
{

    protected $sheet;

    function __construct(Sheet $sheet)
    {
        $this->sheet = $sheet;
    }

    protected function getFileName()
    {
        return 'xl/worksheets/sheet' . $this->sheet->getId() . '.xml';
    }

    public function getHeadXml()
    {
        $xml = $this->getContentTypeXML()
            . '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" '
            . 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" '
            . 'xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" '
            . 'xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">'
            . '<dimension ref="A1:AA100"/>'
            . '<sheetViews>';
        $xml .= '<sheetView' . (
            $this->sheet->getWorkbook()->getActiveSheet() == $this->sheet->getId()
                ? ' tabSelected="1"'
                : ''
            ) . ' workbookViewId="0"';
        if ($this->sheet->getPaneFreez()) {
            $xml .= '>';
            $frozenCellPosition = Cell::getPosition($this->sheet->getPaneFreez());
            $xml .= '<pane xSplit="' . $frozenCellPosition['col'] . '" ySplit="' . ($frozenCellPosition['row'] - 1)
                . '" topLeftCell="' . $this->sheet->getPaneFreez() . '" activePane="bottomRight" state="frozen"/>';
            $xml .= '</sheetView>';
        } else {
            $xml .= '/>';
        }

        $xml .= '</sheetViews>'
            . '<sheetFormatPr baseColWidth="0" defaultColWidth="' . $this->sheet->getDefaultColWidth()
            . '" defaultRowHeight="' . $this->sheet->getDefaultRowHeight()
            . '" customHeight="1" x14ac:dyDescent="0.4"/>';
        $xml .= $this->getCustomColumns();
        $xml .= '<sheetData>';
        return $xml;
    }

    protected function getCustomColumns()
    {
        $xml = '';
        $customColumnWidth = $this->sheet->getColumnWidthList();
        if (!empty($customColumnWidth)) {
            $xml .= '<cols>';
            $minNumber = false;
            $maxNumber = false;
            $lastColumnWidth = false;
            $columnWidthKeys = array_keys($customColumnWidth);
            $lastColumnNumber = end($columnWidthKeys);
            foreach ($customColumnWidth as $columnNumber => $columnWidth) {
                if (!$minNumber) {
                    $minNumber = $columnNumber;
                }
                if ($lastColumnWidth !== false && ($columnWidth != $lastColumnWidth || $maxNumber + 1 != $columnNumber)) {
                    $xml .= '<col min="' . ($minNumber ?: $columnNumber) . '" max="' . ($maxNumber ?: $columnNumber) . '" width="' . $lastColumnWidth . '" customWidth="1"/>';
                    $minNumber = $columnNumber;
                }
                $lastColumnWidth = $columnWidth;
                $maxNumber = $columnNumber;
                if ($columnNumber == $lastColumnNumber) {
                    $xml .= '<col min="' . ($minNumber ?: $columnNumber) . '" max="' . ($maxNumber ?: $columnNumber) . '" width="' . $lastColumnWidth . '" customWidth="1"/>';
                }

            }
            $xml .= '</cols>';
        }
        return $xml;
    }

    public function getRowXml(Row $row)
    {
        $xml = '<row r="' . $row->getIndex() . '" x14ac:dyDescent="0.4">';
        /** @var Cell $cell */
        foreach ($row->getCells() as $cell) {
            $styleId = $this->sheet
                ->getWorkbook()
                ->getStyle()
                ->getStyle(
                    $cell->getFont(),
                    $cell->getFormat(),
                    $cell->getFill(),
                    $cell->getBorder()
                );

            $value = ($cell->getValue() instanceof DateTime)
                ? (25569 + $cell->getValue()->getTimestamp() / 86400)
                : $cell->getValue();
            if ($value === null) {
                $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" />';
            } elseif ($cell->getFormat()) {
                $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="n"><v>' . $value . '</v></c>';
            } elseif ($value[0] === '=' && is_string($value)) {
                $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="s"><f>' . ltrim($value, '=') . '</f></c>';
            } elseif (is_string($value)) {
                $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="s"><v>' . $this->sheet->getWorkbook()->addSharedString($value) . '</v></c>';
            }
        }
        $xml .= '</row>';
        return $xml;
    }

    public function getFootXml()
    {
        $xml = '</sheetData>';
        $mergedCells = $this->sheet->getMergedCells();
        if (!empty($mergedCells)) {
            $xml .= '<mergeCells count="' . count($mergedCells) . '">';
            foreach ($mergedCells as $mergedCell) {
                $xml .= '<mergeCell ref="' . $mergedCell . '"/>';
            }
            $xml .= '</mergeCells>';
        }
        $xml .= '<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>';
        $images = $this->sheet->getImages();
        if (!empty($images)) {
            $xml .= '<drawing r:id="rId1" />';
        }
        $xml .= '</worksheet>';
        return $xml;
    }

    public function writeHeadXml()
    {
        if(filesize($this->sheet->getTmpFile()) === 0){
            file_put_contents($this->sheet->getTmpFile(), $this->getHeadXml(), FILE_APPEND);
        }
    }

    public function writeRowXml(Row $row)
    {
        file_put_contents($this->sheet->getTmpFile(), $this->getRowXml($row), FILE_APPEND);
    }

    public function writeFootXml()
    {
        file_put_contents($this->sheet->getTmpFile(), $this->getFootXml(), FILE_APPEND);
    }

    public function getXml()
    {
        return file_get_contents($this->sheet->getSheetFile());
    }

    public function save(\ZipArchive $zip)
    {
        $fileName = $this->getFileName();
        if (!empty($fileName)) {
            $zip->addFile($this->sheet->getSheetFile(), $fileName);
        }
    }
}