<?php

namespace XLSXLight\Xml;

use XLSXLight\Xml;

class AppXml extends Xml
{
    protected function getFileName()
    {
        return 'docProps/app.xml';
    }

    public function getXml()
    {
        return $this->getContentTypeXML()
            . '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" '
            . 'xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">'
            . '<TotalTime>0</TotalTime>'
            . '</Properties>';
    }
}