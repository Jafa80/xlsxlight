<?php
/**
 * Created by PhpStorm.
 * User: janis
 * Date: 09/09/2017
 * Time: 23:22
 */

namespace XLSXLight\Xml;


use XLSXLight\Workbook;
use XLSXLight\Xml;

class SharedStringsXml extends Xml
{

    protected $workbook;

    function __construct(Workbook $workbook)
    {
        $this->workbook = $workbook;
    }

    protected function getFileName()
    {
        return 'xl/sharedStrings.xml';
    }

    public function getXml()
    {
        $xml = $this->getContentTypeXML()
            . '<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="'
            . $this->workbook->getSharedStringsCount() . '" uniqueCount="' . $this->workbook->getSharedStrings()->count() . '">';
        foreach ($this->workbook->getSharedStrings() as $string => $index) {
            $xml .= '<si><t>' . $string . '</t></si>';
        }
        $xml .= '</sst>';

        return $xml;
    }
}