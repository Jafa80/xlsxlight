<?php

namespace XLSXLight\Xml;


use XLSXLight\Sheet;
use XLSXLight\Workbook;
use XLSXLight\Xml;

class ExRelsXml extends Xml
{

    protected $workbook;

    function __construct(Workbook $workbook)
    {
        $this->workbook = $workbook;
    }

    protected function getFileName()
    {
        return 'xl/_rels/workbook.xml.rels';
    }

    public function getXml()
    {
        $i = 1;
        $sheets = '';
        /** @var Sheet $sheet */
        foreach ($this->workbook->sheets as $sheet) {
            $sheets .= '<Relationship Id="rId' . $i++ . '" '
                . 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" '
                . 'Target="worksheets/sheet' . $sheet->getId() . '.xml"/>';
        }
        return $this->getContentTypeXML()
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">'
            . $sheets . '<Relationship Id="rId' . $i++ . '" '
            . 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" '
            . 'Target="sharedStrings.xml"/>'
            . '<Relationship Id="rId' . $i . '" '
            . 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>'
            . '</Relationships>';
    }
}