<?php

namespace XLSXLight\Xml;


use XLSXLight\Sheet;
use XLSXLight\Workbook;
use XLSXLight\Xml;

class WorkbookXml extends Xml
{
    protected $workbook;

    function __construct(Workbook $workbook)
    {
        $this->workbook = $workbook;
    }

    protected function getFileName()
    {
        return 'xl/workbook.xml';
    }

    public function getXml()
    {
        $sheets = '';
        /** @var Sheet $sheet */
        foreach ($this->workbook->sheets as $sheet) {
            $sheets .= '<sheet name="'
                . $sheet->getTitle()
                . '" sheetId="'
                . $sheet->getId()
                . '" state="visible" r:id="rId'
                . $sheet->getId()
                . '"/>';
        }

        return $this->getContentTypeXML()
            . '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" '
            . 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">'
            . '<fileVersion appName="Calc"/><workbookPr backupFile="false" showObjects="all" '
            . 'date1904="false"/><workbookProtection/>'
            . '<bookViews><workbookView activeTab="0" firstSheet="0" showHorizontalScroll="true" showSheetTabs="true" '
            . 'showVerticalScroll="true" tabRatio="212" windowHeight="8192" windowWidth="16384" xWindow="0" '
            . 'yWindow="0"/></bookViews>'
            . '<sheets>' . $sheets . '</sheets>'
            . '<calcPr iterateCount="100" refMode="A1" iterate="false" iterateDelta="0.001"/>'
            . '</workbook>';
    }
}