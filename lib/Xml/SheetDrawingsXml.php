<?php

namespace XLSXLight\Xml;


use XLSXLight\Image;
use XLSXLight\Sheet;
use XLSXLight\Xml;

class SheetDrawingsXml extends Xml
{

    protected $sheet;

    function __construct(Sheet $sheet)
    {
        $this->sheet = $sheet;
    }

    protected function getFileName()
    {
        return 'xl/drawings/drawing' . $this->sheet->getId() . '.xml';
    }

    public function getXml()
    {
        return $this->getContentTypeXML()
            . '<xdr:wsDr xmlns:xdr="http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing" '
            . 'xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">'
            . $this->getImagesXml()
            . '</xdr:wsDr>';
    }

    protected function getImagesXml()
    {
        $xml = '';
        /** @var Image $image */
        foreach ($this->sheet->getImages() as $image) {
            $xml .= $this->getImageXml($image);
        }
        return $xml;
    }

    protected function getImageXml(Image $image)
    {
        return '<xdr:twoCellAnchor editAs="absolute">'
        . '<xdr:from>'
        . '<xdr:col>' . $image->getColIndex() . '</xdr:col>'
        . '<xdr:colOff>' . $image->getColOffset() . '</xdr:colOff>'
        . '<xdr:row>' . $image->getRowIndex() . '</xdr:row>'
        . '<xdr:rowOff>' . $image->getRowOffset() . '</xdr:rowOff>'
        . '</xdr:from>'

        . '<xdr:to>'
        . '<xdr:col>' . $image->getToColIndex() . '</xdr:col>'
        . '<xdr:colOff>' . $image->getToColOffset() . '</xdr:colOff>'
        . '<xdr:row>' . $image->getToRowIndex() . '</xdr:row>'
        . '<xdr:rowOff>' . $image->getToRowOffset() . '</xdr:rowOff>'
        . '</xdr:to>'

        . '<xdr:pic>'
        . '<xdr:nvPicPr>'
        . '<xdr:cNvPr id="' . $image->getId() . '" name="' . $image->getName() . '"/>'
        . '<xdr:cNvPicPr><a:picLocks noChangeAspect="1"/></xdr:cNvPicPr>'
        . '</xdr:nvPicPr>'

        . '<xdr:blipFill>'
        . '<a:blip xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:embed="rId' . $image->getId() . '">'
        . '<a:extLst>'
        . '<a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">'
        . '<a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>'
        . '</a:ext>'
        . '</a:extLst>'
        . '</a:blip>'
        . '<a:stretch><a:fillRect/></a:stretch>'
        . '</xdr:blipFill>'

        . '<xdr:spPr>'
        . '<a:xfrm>'
        . '<a:off x="0" y="0"/><a:ext cx="0" cy="0"/>'
        . '</a:xfrm>'
        . '<a:prstGeom prst="rect">'
        . '<a:avLst/>'
        . '</a:prstGeom>'
        . '</xdr:spPr>'
        . '</xdr:pic>'

        . '<xdr:clientData/>'
        . '</xdr:twoCellAnchor>';
    }
}