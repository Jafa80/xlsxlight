<?php

namespace XLSXLight\Xml;


use XLSXLight\Border;
use XLSXLight\Font;
use XLSXLight\Style;
use XLSXLight\Xml;

class StylesXml extends Xml
{

    protected $style;

    function __construct(Style $style)
    {
        $this->style = $style;
    }

    protected function getFileName()
    {
        return 'xl/styles.xml';
    }

    public function getXml()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $xml .= '<styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"'
            . ' mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';
        $xml .= $this->getNumberFormatXml();
        $xml .= '<fonts count="' . count($this->style->getFontList()) . '" x14ac:knownFonts="1">';
        $xml .= $this->getFontStylesXml();
        $xml .= '</fonts>';
        $xml .= '<fills count="' . (count($this->style->getFillsList()) + 2) . '">';
        $xml .= '<fill><patternFill patternType="none"/></fill>';
        $xml .= '<fill><patternFill patternType="gray125"/></fill>';
        foreach ($this->style->getFillsList() as $fill) {
            $xml .= '<fill><patternFill patternType="solid"><fgColor rgb="' . strval($fill->color) . '"/><bgColor indexed="64"/></patternFill></fill>';
        }
        $xml .= '</fills>';
        $xml .= '<borders count="' . (count($this->style->getBordersList()) + 1) . '">';
        $xml .= '<border><left/><right/><top/><bottom/><diagonal/></border>';
        foreach ($this->style->getBordersList() as $border) {
            $xml .= $this->getBorderXml($border);
        }
        $xml .= '</borders>';
        $xml .= '<cellStyleXfs count="1">';
        $xml .= '<xf numFmtId="0" fontId="0" fillId="0" borderId="0"/>';
        $xml .= '</cellStyleXfs>';
        $xml .= '<cellXfs count="' . (count($this->style->getStyleSet()) + 1) . '">';
        $xml .= '<xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/>';
        foreach ($this->style->getStyleSet() as $index => $style) {
            $xml .= '<xf numFmtId="' . $style->nformat . '" fontId="' . $style->font
                . '" fillId="' . $style->fill . '" borderId="' . $style->border
                . '" xfId="0" applyBorder="1" applyFill="1"';
            if ($style->alignment) {
                $xml .= ' applyAlignment="1"><alignment';
                foreach ($style->alignment as $type => $value) {
                    $xml .= ' ' . $type . '="' . $value . '"';
                }
                $xml .= '/></xf>';
            } else {
                $xml .= '/>';
            }
        }
        $xml .= '</cellXfs><cellStyles count="1"><cellStyle name="Normal" xfId="0" builtinId="0"/></cellStyles><dxfs count="0"/>'
            . '<tableStyles count="0" defaultTableStyle="TableStyleMedium2" defaultPivotStyle="PivotStyleLight16"/><extLst>'
            . '<ext uri="{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" xmlns:x14="http://schemas.microsoft.com/office/spreadsheetml/2009/9/main">'
            . '<x14:slicerStyles defaultSlicerStyle="SlicerStyleLight1"/></ext></extLst></styleSheet>';
        return $xml;
    }

    public function getNumberFormatXml()
    {
        $numFormats = $this->style->getFormatList();
        $xml = '';
        if (!empty($numFormats)) {
            $xml .= '<numFmts count="' . count($numFormats) . '">';
            foreach ($numFormats as $format) {
                $xml .= '<numFmt numFmtId="' . $format->id . '" formatCode="' . $this->xmlSpecialChars($format->format) . '"/>';
            }
            $xml .= '</numFmts>';
        }
        return $xml;
    }

    public function getFontStylesXml()
    {
        $xml = '';
        /** @var Font $font */
        foreach ($this->style->getFontList() as $font) {
            $xml .= $this->getFontStyleXml($font);
        }
        return $xml;
    }

    public function getFontStyleXml(Font $font)
    {
        $xml = '<font>';
        $xml .= ($font->isBold()) ? '<b/>' : '';
        $xml .= ($font->isUnderline()) ? '<u val="1"/>' : '';
        $xml .= ($font->isItalic()) ? '<i/>' : '';
        $xml .= ($font->isStrikeThrow()) ? '<strike/>' : '';
        $xml .= '<sz val="' . $font->getSize() . '"/>'
            . '<name val="' . $font->getName() . '"/>'
            . '<family val="' . $font->getFamily() . '"/>'
            . '<charset val="' . $font->getCharset() . '"/>'
            . '<scheme val="' . $font->getScheme() . '"/>';
        $xml .= ($font->getColor()) ? '<color rgb="' . $font->getColor() . '"/>' : '';
        $xml .= '</font>';

        return $xml;
    }

    public function getBorderXml(Border $border)
    {
        $thickness = $border->getBorderStyles();
        $colors = $border->getBorderColors();

        $xml = '<border>';
        foreach (['left', 'right', 'top', 'bottom'] as $side) {
            if (!is_null($thickness[$side])) {
                $color = $colors[$side] ? '<color rgb="' . $colors[$side] . '"/>' : '<color auto="1"/>';
                $xml .= '<' . $side . ' style="' . $thickness[$side] . '">' . $color . '</' . $side . '>';
            } else {
                $xml .= '<' . $side . '/>';
            }
        }
        $xml .= '<diagonal/>';
        $xml .= '</border>';

        return $xml;
    }
}