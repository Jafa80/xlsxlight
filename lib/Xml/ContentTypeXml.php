<?php

namespace XLSXLight\Xml;


use XLSXLight\Sheet;
use XLSXLight\Workbook;
use XLSXLight\Xml;

class ContentTypeXml extends Xml
{

    protected $workbook;

    function __construct(Workbook $workbook)
    {
        $this->workbook = $workbook;
    }

    protected function getFileName()
    {
        return '[Content_Types].xml';
    }

    public function getXml()
    {
        $sheets = '';
        $drawings = '';
        /** @var Sheet $sheet */
        foreach ($this->workbook->sheets as $sheet) {
            $sheets .= '<Override PartName="/xl/worksheets/sheet' . $sheet->getId()
                . '.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"/>';
            $images = $sheet->getImages();
            if (!empty($images)) {
                $drawings .= '<Override PartName="/xl/drawings/drawing' . $sheet->getId()
                    . '.xml" ContentType="application/vnd.openxmlformats-officedocument.drawing+xml"/>';
            }
        }

        return $this->getContentTypeXML()
            . '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">'
            . '<Default Extension="xml" ContentType="application/xml"/>'
            . '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>'
            . '<Default Extension="jpeg" ContentType="image/jpeg"/><Default Extension="png" ContentType="image/png"/>'
            . '<Default Extension="jpg" ContentType="application/octet-stream"/>'
            . '<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>'
            . $sheets
            . '<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>'
            . $drawings
            . '<Override PartName="/xl/sharedStrings.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"/>'
            . '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>'
            . '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>'
            . '</Types>';
    }
}