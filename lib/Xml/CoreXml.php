<?php
/**
 * Created by PhpStorm.
 * User: janis
 * Date: 08/09/2017
 * Time: 22:26
 */

namespace XLSXLight\Xml;


use XLSXLight\Workbook;
use XLSXLight\Xml;

class CoreXml extends Xml
{
    protected $workbook;

    function __construct(Workbook $workbook)
    {
        $this->workbook = $workbook;
    }

    protected function getFileName()
    {
        return 'docProps/core.xml';
    }

    public function getXML()
    {
        return $this->getContentTypeXML()
            . '<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" '
            . 'xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" '
            . 'xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
            . '<dcterms:created xsi:type="dcterms:W3CDTF">' . date('Y-m-d\TH:i:s.00\Z') . '</dcterms:created>'
            . '<dc:creator>' . $this->workbook->getAuthor() . '</dc:creator>'
            . '<cp:revision>0</cp:revision>'
            . '</cp:coreProperties>';
    }
}