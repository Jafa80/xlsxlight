<?php

namespace XLSXLight\Xml;


use XLSXLight\Image;
use XLSXLight\Sheet;
use XLSXLight\Xml;

class SheetDrawingRelsXml extends Xml
{

    protected $sheet;

    function __construct(Sheet $sheet)
    {
        $this->sheet = $sheet;
    }

    protected function getFileName()
    {
        return 'xl/drawings/_rels/drawing' . $this->sheet->getId() . '.xml.rels';
    }

    public function getXml()
    {
        $xml = $this->getContentTypeXML()
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">';
        /** @var  Image $image */
        foreach ($this->sheet->getImages() as $image) {
            $xml .= '<Relationship Id="rId' . $image->getId()
                . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="../media/'
                . $image->getXlsImage() . '"/>';
        }
        $xml .= '</Relationships>';

        return $xml;
    }
}